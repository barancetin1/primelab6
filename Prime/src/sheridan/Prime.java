package sheridan;

public class Prime {
	
	
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + Prime.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + Prime.isPrime( 35 ) );		
		System.out.println( "Number 36 is prime? " + Prime.isPrime( 36 ) );			
		
		//Team member one: baran's line of code:
		System.out.println( "Number 37 is prime? " + Prime.isPrime( 37 ) );		
		
		//Team member two: Garrett's line of code:
		System.out.println( "Number 38 is prime? " + Prime.isPrime( 38 ) );	
		
		//Team member three: Richa's line of code:
				System.out.println( "Number 39 is prime? " + Prime.isPrime( 39 ) );	
				
		//Team member one: Baran's second line of code:
	    System.out.println( "Number 40 is prime? " + Prime.isPrime( 40 ) );	
	    
		//Team member two: Garrett's second line of code:
		System.out.println( "Number 41 is prime? " + Prime.isPrime( 41 ) );	
		
		//Team member three: Richa's second line of code:
		System.out.println( "Number 43 is prime? " + Prime.isPrime( 43) );	
		
	}

}
